#include <iostream>

using namespace std;

int main(){
    int a, b, c, d, s, m;
    cin>>a>>b>>c>>d;

    s = a + b;
    m = c + d;
    if(s % 2 == 0 && m % 2 == 0){
        cout<<"Yes"<<endl;
    }
    if(s % 2 != 0 && m % 2 != 0){
        cout<<"Yes"<<endl;
    }
    else{
        cout<<"No"<<endl;
    }
    return 0;
}