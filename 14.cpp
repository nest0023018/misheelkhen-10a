#include <iostream>

using namespace std;

int main(){
    int a, b, c, s;
    cin>>a>>b>>c;

    s = sqrt(4 * a * a * b * b - (a * a + b * b - c * c) * (a * a + b * b - c * c));
    s = s / 4;

    cout<<s<<endl;
    return 0;
}