#include <iostream>

using namespace std;

int main(){
    int a, n, d;
    cin>>a;

    d = 0;
    while (a > 0){
        n = a % 10;
        a = a / 10;
        d = d + n;
    }

    cout<<d<<endl;
    return 0;
}